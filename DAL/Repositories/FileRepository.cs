﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class FileRepository : IFileRepository
    {
        protected static string TargetDir => $@"{Directory.GetParent(Directory.GetCurrentDirectory()).FullName}\DAL\Context\Photos\";

        /// <summary>
        /// Returns decoded base64 string of photo file by the specified path.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task<string> GetBase64Async(string fileName)
        {
            string path = $"{CreatePath(fileName)}";
            if (fileName == null || !File.Exists(path))
                return null;

            string encoded;
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                byte[] bytes = new byte[fs.Length];
                await fs.ReadAsync(bytes, 0, bytes.Length);
                encoded = Convert.ToBase64String(bytes);
            }
            return encoded;   
        }

        public async Task<IEnumerable<string>> GetBase64FilesAsync(IEnumerable<string> fileNames)
        {
            List<string> encoded = new List<string>();

            foreach (var name in fileNames)
                encoded.Add(await GetBase64Async(name));

            return encoded;
        }

        /// <summary>
        /// Decodes a base64 string to a photo file and saves to the db.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="base64"></param>
        /// <returns></returns>
        public async Task SaveFileAsync(string fileName, string base64)
        {
            var bytes = Convert.FromBase64String(base64.Split(',').Last());

            using (FileStream fs = new FileStream(CreatePath(fileName), FileMode.Create))
                await fs.WriteAsync(bytes);
        }

        /// <summary>
        /// Deletes the file at the specified path.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task DeleteFileAsync(string fileName)
        {
             if (File.Exists(CreatePath(fileName)))
                await Task.Run(() => File.Delete(CreatePath(fileName)));
        }

        private string CreatePath(string fileName) => $"{TargetDir}{fileName}";
    }
}
