﻿using DAL.Context;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserRepository : EntityRepositoryBase<User>
    {
        public UserRepository(ApplicationContext context) : base(context) { }

        public async Task<User> FindUserByIdAsync(string userId, bool trackChanges = false) =>
                await GetByCondition(u => u.Id.Equals(userId), trackChanges: true).FirstOrDefaultAsync();
    }
}
