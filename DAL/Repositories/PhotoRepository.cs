﻿using DAL.Context;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class PhotoRepository : EntityRepositoryBase<Photo>
    {
        public PhotoRepository(ApplicationContext context) : base(context) { }

        public async Task<Photo> FindPhoto(string userId, int albumId, int photoId, bool trackChanges) =>
            await GetByCondition(p => p.Album.AuthorId.Equals(userId) && p.AlbumId == albumId && p.Id == photoId, trackChanges).FirstOrDefaultAsync();

        public IQueryable<Photo> FindPhotosByAuthor(string authorId, bool trackChanges) =>
            GetByCondition(p => p.Album.AuthorId.Equals(authorId), trackChanges);
    }
}
