﻿using DAL.Context;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class AlbumRepository : EntityRepositoryBase<Album>
    {
        public AlbumRepository(ApplicationContext context) : base(context) { }

        public async Task<Album> FindAlbumForUserAsync(string userId, int albumId, bool trackChanges = false) => 
            await GetByCondition(a => a.Id == albumId && a.AuthorId.Equals(userId), trackChanges).FirstOrDefaultAsync();

        public IQueryable<Album> FindAlbumsByUser(string userId, bool trackChanges) =>
            GetByCondition(a => a.AuthorId.Equals(userId), trackChanges);

        public IQueryable<Album> GetAlbumsByUserWithDetails(string userId) => _context.Albums
            .Include(a => a.Photos)
            .ThenInclude((Photo p) => p.Ratings)
            .Include(a => a.Author)
            .Where(a => a.AuthorId.Equals(userId));
    }
}
