﻿using DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public abstract class EntityRepositoryBase<TEntity> where TEntity: class //where TEntity: EntityBase
    {
        protected readonly ApplicationContext _context;

        protected EntityRepositoryBase(ApplicationContext context) => _context = context;

        public virtual IQueryable<TEntity> GetAll(bool trackChanges) => trackChanges ?
            _context.Set<TEntity>() : _context.Set<TEntity>().AsNoTracking();

        public virtual async Task<TEntity> GetByIdAsync(int id) => await _context.Set<TEntity>().FindAsync(id);

        public virtual IQueryable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> expression, bool trackChanges) =>
            GetAll(trackChanges).Where(expression);

        public virtual EntityEntry<TEntity> Add(TEntity entity) => _context.Set<TEntity>().Add(entity);

        public virtual async Task<EntityEntry<TEntity>> AddAsync(TEntity entity) => await _context.Set<TEntity>().AddAsync(entity);

        public virtual void Update(TEntity entity) => _context.Set<TEntity>().Update(entity);

        public virtual void Delete(TEntity entity) => _context.Set<TEntity>().Remove(entity);
    }
}
