﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IFileRepository
    {
        Task<IEnumerable<string>> GetBase64FilesAsync(IEnumerable<string> fileNames);
        Task<string> GetBase64Async(string fileName);
        Task SaveFileAsync(string fileName, string base64);
        Task DeleteFileAsync(string fileName);
    }
}
