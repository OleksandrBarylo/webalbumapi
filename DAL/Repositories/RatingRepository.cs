﻿using DAL.Context;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class RatingRepository : EntityRepositoryBase<Rating>
    {
        public RatingRepository(ApplicationContext context) : base(context) { }

        public async Task<Rating> FindRatingForPhotoAsync(int photoId, int ratingId, bool trackChanges) => 
            await GetByCondition(r => r.RatedPhotoId == photoId && r.Id == ratingId, trackChanges).OrderBy(x => x).FirstOrDefaultAsync();

        public async Task<Rating> FindRatingForAuthorAsync(string rateAuthorId, int ratingId, bool trackChanges) =>
            await GetByCondition(r => r.RateAuthorId.Equals(rateAuthorId) && r.Id == ratingId, trackChanges).FirstOrDefaultAsync();

        public IQueryable<Rating> FindAllRatingsForPhoto(int photoId, bool trackChanges) =>
            GetByCondition(r => r.RatedPhotoId == photoId, trackChanges);

        public IQueryable<Rating> FindAllRatingsLeftByUser(string userId, bool trackChanges) =>
            GetByCondition(r => r.RateAuthorId.Equals(userId), trackChanges);
    }
}
