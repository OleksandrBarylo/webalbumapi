﻿using DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace DAL.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        PhotoRepository PhotoRepository { get; }
        RatingRepository RatingRepository { get; }
        AlbumRepository AlbumRepository { get; }
        UserRepository UserRepository { get; }
        IFileRepository FileRepository { get; }
        Task SaveChangesAsync();
    }
}
