﻿using DAL.Context;
using DAL.Repositories;
using System.Threading.Tasks;

namespace DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private readonly ApplicationContext _context;
        private PhotoRepository _photoRepository;
        private RatingRepository _ratingRepository;
        private AlbumRepository _albumRepository;
        private UserRepository _userRepository;
        private IFileRepository _fileRepository;
        public UnitOfWork(ApplicationContext context) => _context = context;

        public PhotoRepository PhotoRepository => _photoRepository ??= new PhotoRepository(_context);

        public RatingRepository RatingRepository => _ratingRepository ??= new RatingRepository(_context);

        public AlbumRepository AlbumRepository => _albumRepository ??= new AlbumRepository(_context);

        public UserRepository UserRepository => _userRepository ??= new UserRepository(_context);

        public IFileRepository FileRepository => _fileRepository ??= new FileRepository();

        /// <summary>
        /// Saves changes made to the context.
        /// </summary>
        /// <returns></returns>
        public async Task SaveChangesAsync() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();
    }
}
