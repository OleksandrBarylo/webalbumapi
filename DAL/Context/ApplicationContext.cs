﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using DAL.Configuration;

namespace DAL.Context
{
    public class ApplicationContext : IdentityDbContext<User> 
    {
        public ApplicationContext(DbContextOptions options) : base(options) 
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<IdentityDbContext<User>>());
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new RolesConfiguration())
                .ApplyConfiguration(new AlbumConfiguration())
                .ApplyConfiguration(new PhotoConfiguration());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            //await new SeedData(_userManager).DropCreateAsync(this);
            optionsBuilder.UseLazyLoadingProxies();
        }

        public DbSet<Photo> Photos { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Rating> Ratings { get; set; }
    }
}
