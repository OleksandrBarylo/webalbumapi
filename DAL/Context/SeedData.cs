﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace DAL.Context
{
    public class SeedData
    {
        private readonly UserManager<User> _userManager;
        public SeedData(UserManager<User> userManager) => _userManager = userManager;

        /// <summary>
        /// Seeding initial test data to the database.
        /// </summary>
        /// <param name="context"></param>
        public async Task DropCreateAsync(ApplicationContext context)
        {
            await context.Database.EnsureDeletedAsync();
            await context.Database.EnsureCreatedAsync();

            User user1 = new User { FirstName = "Alex", LastName = "Super", UserName = "Admin", Email = "sample1@gmail.com" },
                user2 = new User { FirstName = "Otto", LastName = "Nowak", UserName = "Ottoj", Email = "sample2@gmail.com" },
                user3 = new User { FirstName = "Olga", LastName = "Persha", UserName = "Olyasha2", Email = "sample3@gmail.com" };

            await _userManager.CreateAsync(user1, "123456t");
            await _userManager.CreateAsync(user2, "hello2");
            await _userManager.CreateAsync(user3, "super123");

            await _userManager.AddToRolesAsync(user1, new string[] { "Author", "Admin" });
            await _userManager.AddToRoleAsync(user2, "Author");
            await _userManager.AddToRoleAsync(user3, "Author");

            var albums = new Album[]
            {
                new Album { Author = user2, Name = "My first album", Description = "Album with moments of my life." },
                new Album { Author = user2, Name = "Private album", Description = "Album with personal photos." },
                new Album { Author = user3, Name = "Funny album", Description = "Album with funny moments." },
            };

            await context.Albums.AddRangeAsync(albums);

            await context.SaveChangesAsync();
        }
    }
}
