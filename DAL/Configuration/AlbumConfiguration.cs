﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configuration
{
    public class AlbumConfiguration : IEntityTypeConfiguration<Album>
    {
        public void Configure(EntityTypeBuilder<Album> builder)
        {
            builder.HasOne(a => a.Author)
                .WithMany(c => c.Albums)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(a => a.AuthorId);
        }
    }
}
