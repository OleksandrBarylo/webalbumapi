﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configuration
{
    /// <summary>
    /// Configures application users' roles.
    /// </summary>
    public class RolesConfiguration : IEntityTypeConfiguration<IdentityRole>
    {
        public void Configure(EntityTypeBuilder<IdentityRole> builder) => builder.HasData(
            new IdentityRole { Name = "Guest", NormalizedName = "GUEST" },
            new IdentityRole { Name = "Author", NormalizedName = "AUTHOR" },
            new IdentityRole { Name = "Admin", NormalizedName = "ADMIN" });
    }
}
