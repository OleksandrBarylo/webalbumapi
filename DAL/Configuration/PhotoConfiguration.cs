﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configuration
{
    public class PhotoConfiguration : IEntityTypeConfiguration<Photo>
    {
        public void Configure(EntityTypeBuilder<Photo> builder)
        {
            builder.HasOne(p => p.Album)
                .WithMany(a => a.Photos)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(p => p.AlbumId);
        }
    }
}
