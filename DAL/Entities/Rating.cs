﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Rating : IEntity<int>
    {
        public int Id { get; set; }
        [Required]
        /// <summary>
        /// Rate 1 through 5.
        /// </summary>
        public int Rate { get; set; }
        /// <summary>
        /// Optional comment or review.
        /// </summary>
        public string Comment { get; set; }
        [Required]
        public int RatedPhotoId { get; set; }
        /// <summary>
        /// Photo that was rated.
        /// </summary>
        public virtual Photo RatedPhoto { get; set; }
        [Required]
        public string RateAuthorId { get; set; }
        /// <summary>
        /// User who rated the photo.
        /// </summary>
        public virtual User RateAuthor { get; set; }
    }
}
