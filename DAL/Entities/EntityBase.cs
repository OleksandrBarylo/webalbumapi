﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    /// <summary>
    /// Base abstract class for all EF entities providing Id property of type int.
    /// </summary>
    public interface IEntity<TId>
    {
        /// <summary>
        /// Entity primary key.
        /// </summary>
        [Key]
        public TId Id { get; set; }
    }
}
