﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Photo : IEntity<int>
    {
        public int Id { get; set; }
        /// <summary>
        /// Optional photo short description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Path to the photo file on the hard drive.
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Date and time of uploading.
        /// </summary>
        public DateTime UploadDate { get; set; }
        /// <summary>
        /// Photo ratings by users.
        /// </summary>
        public virtual ICollection<Rating> Ratings { get; set; } = new HashSet<Rating>();
        /// <summary>
        /// Foreigh key, author's id.
        /// </summary> 
        [Required]
        public int AlbumId { get; set; }
        public virtual Album Album { get; set; }
    }
}
