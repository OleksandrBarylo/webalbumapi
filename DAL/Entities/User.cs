﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class User : IdentityUser, IEntity<string>
    {
        /// <summary>
        /// User's first name.
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// User's last name.
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Account registration date and time.
        /// </summary>
        public DateTime RegistrationDate { get; set; }
        /// <summary>
        /// User's albums.
        /// </summary>
        [Required]
        public virtual ICollection<Album> Albums { get; set; }
    }
}
