﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Album : IEntity<int>
    {
        public int Id { get; set; }
        /// <summary>
        /// Album name.
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Album short description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Whether the other users can view and rate the album photos or not.
        /// </summary>
        public bool IsPrivate { get; set; }
        /// <summary>
        /// Foreign key, creator's id.
        /// </summary>
        [Required]
        public string AuthorId { get; set; }
        
        /// <summary>
        /// User who created the album but not obviously author of all photos.
        /// </summary>
        [Required]
        public virtual User Author { get; set; }
        /// <summary>
        /// Photos in album.
        /// </summary>
        [Required]
        public virtual ICollection<Photo> Photos { get; set; } = new HashSet<Photo>();
    }
}
