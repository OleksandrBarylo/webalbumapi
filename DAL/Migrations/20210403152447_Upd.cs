﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Upd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5b23c9ce-5eac-4ff5-9195-65e304ca71f8");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "608988bb-aff9-4158-9a21-f61cd465996f");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f4058520-a78a-484c-b9c7-4a28b079bafd");

            migrationBuilder.AlterColumn<string>(
                name: "FileName",
                table: "Photos",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "b850b594-4562-416c-b57a-3dae5c080c23", "1624a57e-11ed-4e26-b226-31cd95b1b328", "Guest", "GUEST" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "57b4c960-a42d-4532-9b8e-790ad469cc38", "223f82a1-277a-4da4-9070-eab2e9d2bd4a", "Author", "AUTHOR" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "2d5bf45d-637f-47f3-a13f-252178938485", "2041cc97-1420-4c0a-98c8-1b838f011a9d", "Admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2d5bf45d-637f-47f3-a13f-252178938485");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "57b4c960-a42d-4532-9b8e-790ad469cc38");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b850b594-4562-416c-b57a-3dae5c080c23");

            migrationBuilder.AlterColumn<string>(
                name: "FileName",
                table: "Photos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "f4058520-a78a-484c-b9c7-4a28b079bafd", "c75bf875-d794-4a8a-8d8f-7e60f8aafa84", "Guest", "GUEST" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "5b23c9ce-5eac-4ff5-9195-65e304ca71f8", "d4e05356-d32e-4793-b9d5-b564c24f00dd", "Author", "AUTHOR" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "608988bb-aff9-4158-9a21-f61cd465996f", "50a50e78-6d27-4a3e-bae8-cc0a29255e8c", "Admin", "ADMIN" });
        }
    }
}
