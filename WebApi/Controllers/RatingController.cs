﻿using BLL.Exceptions;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.DataTransferObjects.RatingDtos;
using WebApi.Filters;

namespace WebApi.Controllers
{
    [Route("api/[controller]s")]
    public class RatingController : ControllerBase, IPartialAdminCrudController<RatingDto, RatingToAddUpdateDto, int>
    {
        private readonly IRatingService _ratingService;

        public RatingController(IRatingService ratingService)
        {
            _ratingService = ratingService;
        }

        /// <summary>
        /// Returns a single rating.
        /// </summary>
        /// <param name="id">Raating Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{id:int}", Name = "GetRating")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _ratingService.GetAsync(id));
        }

        /// <summary>
        /// Returns the collection of ratings.
        /// </summary>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetCollection([FromQuery] RequestParameters parameters)
        {
            return Ok(await _ratingService.GetCollectionAsync(parameters));
        }

        /// <summary>
        /// Updates the rating specified by Id.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] RatingToAddUpdateDto updateDto)
        {
            return Ok(await _ratingService.UpdateAsync(id, updateDto));
        }

        /// <summary>
        /// Updates the rating which belongs to the user sent the request.
        /// The user Id is specified from the JWT in the Authorize header.
        /// </summary>
        /// <param name="id">Rating to update Id</param>
        /// <param name="ratingToUpdate">Rating DTO for update.</param>
        /// <returns></returns>
        [Authorize(Roles = "Author")]
        [HttpPut("my/{id:int}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> UpdateMyRating(int id, [FromBody] RatingToAddUpdateDto ratingToUpdate)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return Ok(await _ratingService.UpdateRatingOfUserAsync(userId, id, ratingToUpdate));
        }

        /// <summary>
        /// Updates the rating.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="ratingAuthorId">Author Id of the rating</param>
        /// <param name="ratingId">Rating Id</param>
        /// <param name="ratingToUpdate">Rating DTO fro update</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{ratingAuthorId:alpha}/{ratingId:int}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> UpdateUserRating(string ratingAuthorId, int ratingId, [FromBody] RatingToAddUpdateDto ratingToUpdate)
        {
            return Ok(await _ratingService.UpdateRatingOfUserAsync(ratingAuthorId, ratingId, ratingToUpdate));
        }

        /// <summary>
        /// Adds a rating to a photo.
        /// The user Id is specified from the JWT in the Authorize header.
        /// </summary>
        /// <param name="photoId">Photo Id to be rated</param>
        /// <param name="ratingToAdd">Rating DTO to be added</param>
        /// <returns></returns>
        [Authorize(Roles = "Author")]
        [HttpPost("me/{photoId:int}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> AddMyRating(int photoId, [FromBody] RatingToAddUpdateDto ratingToAdd)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            var result = await _ratingService.AddRatingAsync(userId, photoId, ratingToAdd);

            return CreatedAtRoute("GetRating", new { id = result.Id }, result);
        }

        /// <summary>
        /// Deletes the rating by Id.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _ratingService.DeleteAsync(id));
        }

        /// <summary>
        /// Deletes the rating left by user sent the request.
        /// The user Id is specified from the JWT in the Authorize header.
        /// </summary>
        /// <param name="rateAuthorId">Rating author Id</param>
        /// <param name="ratingId">Rating to delete Id</param>
        /// <returns></returns>
        [Authorize(Roles = "Author")]
        [ServiceFilter(typeof(ValidationFilter))]
        [HttpDelete("{ratingId:int}")]
        public async Task<IActionResult> DeleteMyRating(int ratingId)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return Ok(await _ratingService.DeleteRatingOfUserAsync(userId, ratingId));
        }

        /// <summary>
        /// Deletes the rating left by user specified by Id.
        /// The rating is also specified by Id and must belong to the specified user.
        /// </summary>
        /// <param name="rateAuthorId"></param>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [ServiceFilter(typeof(ValidationFilter))]
        [HttpDelete("{rateAuthorId:alpha}/{ratingId:int}")]
        public async Task<IActionResult> DeleteUserRating(string rateAuthorId, int ratingId)
        {
            return Ok(await _ratingService.DeleteRatingOfUserAsync(rateAuthorId, ratingId));
        }
    }
}

#region Backup
#endregion
