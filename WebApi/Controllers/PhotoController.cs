﻿using BLL.DataTransferObjects.PhotoDtos;
using BLL.Exceptions;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.Filters;

namespace WebApi.Controllers
{
    [Route("api/[controller]s")]
    public class PhotoController : ControllerBase, IPartialAdminCrudController<PhotoDto, PhotoToUpdateDto, int>
    {
        private readonly IPhotoService _photoService;
        
        public PhotoController(IPhotoService photoService) => _photoService = photoService;

        /// <summary>
        /// Return a single photo DTO.
        /// </summary>
        /// <param name="id">Photo Id</param>
        /// <returns></returns>
        [HttpGet("{id:int}", Name = "GetPhoto")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _photoService.GetAsync(id));
        }

        /// <summary>
        /// Returns a collection of photo DTOs.
        /// </summary>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetCollection([FromQuery] RequestParameters parameters)
        {
            return Ok(await _photoService.GetCollectionAsync(parameters));
        }

        /// <summary>
        /// Updates a single photo.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="id">Photo Id</param>
        /// <param name="updateDto">Photo DTO for update</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] PhotoToUpdateDto updateDto)
        {
            return Ok(await _photoService.UpdateAsync(id, updateDto));
        }

        /// <summary>
        /// Updates a photo which belongs to a specific album.
        /// </summary>
        /// <param name="albumId">AlbumId</param>
        /// <param name="photoId">Photo Id</param>
        /// <param name="photoToUpdate">Photo DTO for update</param>
        /// <returns></returns>
        [Authorize]
        [ServiceFilter(typeof(ValidationFilter))]
        [HttpPut("my/{photoId:int}")]
        public async Task<IActionResult> UpdatePhoto(int albumId, int photoId, [FromBody] PhotoToUpdateDto photoToUpdate)
        {
            string authorId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            var result = await _photoService.UpdateAuthoredPhotoAsync(authorId, photoId, photoToUpdate);

            return Ok(result);
        }

        /// <summary>
        /// Adds a photo to an album.
        /// </summary>
        /// <param name="albumId">Album Id</param>
        /// <param name="photoDto">Photo DTO to add</param>
        /// <returns></returns>
        [Authorize]
        [ServiceFilter(typeof(ValidationFilter))]
        [HttpPost("album/{albumId:int}")]
        public async Task<IActionResult> AddPhotoToAlbum(int albumId, [FromBody] PhotoToAddDto photoDto)
        {
            string authorId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

                var result = await _photoService.AddPhotoToAuthorAlbumAsync(authorId, albumId, photoDto);
           

            return CreatedAtRoute("GetPhoto", new { id = result.Id }, result);
        }

        /// <summary>
        /// Deletes a single photo.
        /// Accessible for administrators.
        /// <param name="id">Photo Id</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _photoService.DeleteAsync(id));
        }

        /// <summary>
        /// Deletes a photo which belongs to a specific album.
        /// </summary>
        /// <param name="albumId">Album Id</param>
        /// <param name="photoId">Photo Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("{albumId:int}/{photoId:int}")]
        public async Task<IActionResult> DeletePhoto(int albumId, int photoId)
        {
            string authorId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            var result = await _photoService.RemovePhotoFromAuthorAlbumAsync(authorId, albumId, photoId);

            return Ok(result);
        }

        /// <summary>
        /// Returns a single rating which belongs to a photo.
        /// </summary>
        /// <param name="photoId">Photo Id</param>
        /// <param name="ratingId">Rating Id</param>
        /// <returns></returns>
        [HttpGet("{photoId:int}/ratings/{ratingId:int}", Name = nameof(GetRatingOfPhoto))]
        public async Task<IActionResult> GetRatingOfPhoto(int photoId, int ratingId)
        {
            return Ok(await _photoService.GetRatingForPhotoAsync(photoId, ratingId));
        }

        /// <summary>
        /// Returns a collection of ratings which belong to the photo.
        /// </summary>
        /// <param name="photoId">Photo Id</param>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [HttpGet("{photoId:int}/ratings")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetRatingsOfPhoto(int photoId, [FromQuery] RequestParameters parameters)
        {
            return Ok(await _photoService.GetRatingsForPhotoAsync(photoId, parameters));
        }
    }
}

#region Backup
#endregion