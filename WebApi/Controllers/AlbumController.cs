﻿using BLL.DataTransferObjects.AlbumDtos;
using BLL.Exceptions;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.Filters;

namespace WebApi.Controllers
{
    [Route("api/[controller]s")]
    public class AlbumController : ControllerBase, IPartialAdminCrudController<AlbumDto, AlbumToAddUpdateDto, int>
    {
        private readonly IAlbumService _albumService;

        public AlbumController(IAlbumService albumService) => _albumService = albumService;

        /// <summary>
        /// Returns a single album DTO by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}", Name = "GetAlbum")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _albumService.GetAsync(id));
        }

        /// <summary>
        /// Returns a colection of album DTOs.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetCollection([FromQuery] RequestParameters parameters)
        {
            return Ok(await _albumService.GetCollectionAsync(parameters));
        }

        /// <summary>
        /// Returns the collection of photos of the album specified by the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet("{id:int}/photos")]
        public async Task<IActionResult> GetPhotosOfAlbum(int id, [FromQuery] RequestParameters parameters)
        {
            return Ok(await _albumService.GetPhotosByAlbumAsync(id, parameters));
        }

        /// <summary>
        /// Updates the album with the specified Id.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] AlbumToAddUpdateDto updateDto)
        {
            return Ok(await _albumService.UpdateAsync(id, updateDto));
        }

        /// <summary>
        /// Updates the album of the user sent the request.
        /// The album is specified by Id.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="albumToUpdate"></param>
        /// <returns></returns>
        [Authorize]
        [ServiceFilter(typeof(ValidationFilter))]
        [HttpPut("my/{id:int}")]
        public async Task<IActionResult> UpdateMyAlbum(int id, [FromBody] AlbumToAddUpdateDto albumToUpdate)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            var result = await _albumService.UpdateAlbumOfUserAsync(userId, id, albumToUpdate);

            return Ok(result);
        }

        /// <summary>
        /// Deletes the album with the specified Id.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _albumService.DeleteAsync(id));
        }

        /// <summary>
        /// Deletes the album with the specified Id.
        /// Accessible for album authors.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("my/{id:int}")]
        public async Task<IActionResult> DeleteMyAlbum(int id)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            await _albumService.DeleteAlbumAsync(userId, id);

            return NoContent();
        }

        /// <summary>
        /// Adds an album with photos for the user sent the request.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("my")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> AddAlbum([FromBody] AlbumToAddUpdateDto albumToAdd)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return await AddAlbum(userId, albumToAdd);
        }

        /// <summary>
        /// Adds an album with photos for the user specified by Id.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost("{userId:alpha}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> AddAlbum(string userId, [FromBody] AlbumToAddUpdateDto albumToAdd)
        {
            var result = await _albumService.AddAlbumAsync(userId, albumToAdd);

            return CreatedAtRoute("GetAlbum", new { id = result.Id }, result);
        }   
    }
}

#region Backup
//[Authorize]
//[HttpGet("albums/{albumId:int}")]
//public async Task<IActionResult> GetAlbum(int albumId)
//{
//    string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
//        throw new UserException("The user id claim not found.") { StatusCode = 401 };

//    return await GetAlbum(userId, albumId);
//}

//[Authorize]
//[HttpGet("{userId:alpha}/albums/{albumId:int}")]
//public async Task<IActionResult> GetAlbum(string userId, int albumId)
//{
//    return Ok(await _albumService.GetAlbumWithPhotosForUserAsync(userId, albumId));
//}
#endregion