﻿using BLL.Exceptions;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.DataTransferObjects.UserDtos;
using WebApi.Filters;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]s")]
    public class UserController : ControllerBase, IPartialAdminCrudController<UserDto, UserToRegistrateUpdateDto, Guid>
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Returns a single of user profiles.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return Ok(await _userService.GetAsync(id.ToString()));
        }

        /// <summary>
        /// Returns the current profile if the user is logged in.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("me")]
        public async Task<IActionResult> GetMyProfile()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return Ok(await _userService.GetAsync(userId));
        }

        /// <summary>
        /// Returns the collection of user profiles.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetCollection([FromQuery] RequestParameters parameters)
        {
            return Ok(await _userService.GetCollectionAsync(parameters));
        }

        /// <summary>
        /// Updates the user profile personal information.
        /// Accessible for admins.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="updateDto">User DTO for update</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> Update(Guid id, [FromBody] UserToRegistrateUpdateDto updateDto)
        {
            return Ok(await _userService.UpdateAsync(id.ToString(), updateDto));
        }

        /// <summary>
        /// Updates the profile personal information of the user sent the request.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="userForUpdate">User DTO for update</param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("me")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> UpdateMyProfile([FromBody] UserToRegistrateUpdateDto userForUpdate)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return Ok(await _userService.UpdateAsync(userId, userForUpdate));
        }

        /// <summary>
        /// Deletes the album.
        /// Accessible for administrators.
        /// </summary>
        /// <param name="id">Album Id</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await _userService.DeleteAsync(id.ToString()));
        }

        /// <summary>
        /// Returns a single album of the user.
        /// </summary>
        /// <param name="userId">Album author Id</param>
        /// <param name="albumId">Album Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{userId}/albums/{albumId:int}")] 
        public async Task<IActionResult> GetAlbumOfUser(Guid userId, int albumId)
        {
            return Ok(await _userService.GetAlbumWithPhotosOfUserAsync(userId.ToString(), albumId));
        }

        /// <summary>
        /// Returns a single album of the user sent the request.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="id">Album id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("me/albums/{id:int}")]
        public async Task<IActionResult> GetAlbumOfUser(int id)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return await GetAlbumOfUser(Guid.Parse(userId), id);
        }

        /// <summary>
        /// Returns collection of user's most rated photos.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="count">Maximum number of photos to be returned</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("me/mostRatedPhotos/{count:int}")]
        public async Task<IActionResult> GetMyMostRatedPhotos(int count)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return Ok(await _userService.GetMostRatedPhotosAsync(userId, count));
        }

        /// <summary>
        /// Returns collection of user's most rated photos.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="count">Maximum number of photos to be returned</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{userId}/mostRatedPhotos/{count:int}")]
        public async Task<IActionResult> GetUserMostRatedPhotos(Guid userId, int count)
        {
            return Ok(await _userService.GetMostRatedPhotosAsync(userId.ToString(), count));
        }

        [Authorize]
        [HttpGet("me/photosWithHighestRate/{count:int}")]
        public async Task<IActionResult> GetMyPhotosWithHighestRate(int count)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return Ok(await _userService.GetPhotosWithHighestRateAsync(userId, count));
        }

        [Authorize]
        [HttpGet("{userId}/photosWithHighestRate/{count:int}")]
        public async Task<IActionResult> GetMyPhotosWithHighestRate(Guid userId, int count)
        {
            return Ok(await _userService.GetPhotosWithHighestRateAsync(userId.ToString(), count));
        }

        /// <summary>
        /// Returns collection of photos of author.
        /// </summary>
        /// <param name="id">Author Id</param>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [HttpGet("{id:alpha}/photos")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetPhotosOfAuthor(string id, [FromQuery] RequestParameters parameters)
        {
            return Ok(await _userService.GetPhotosByAuthorAsync(id, parameters));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [Authorize(Roles = "Author")]
        [HttpGet("me/ratings")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetRatingsOfUser([FromQuery] RequestParameters parameters)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return Ok(await _userService.GetRatingsLeftByUserAsync(userId, parameters));
        }

        /// <summary>
        /// Returns collection of albums of the user.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [HttpGet("{id:alpha}/ratings")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetRatingsOfUser(string id, [FromQuery] RequestParameters parameters)
        {
            return Ok(await _userService.GetRatingsLeftByUserAsync(id, parameters));
        }

        /// <summary>
        /// Returns collection of albums of user sent the request.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("me/albums")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetAlbumsOfUser([FromQuery] RequestParameters parameters)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return await GetAlbumsOfUser(userId, parameters);
        }

        [Authorize]
        [HttpGet("{id:alpha}/albums")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetAlbumsOfUser(string id, [FromQuery] RequestParameters parameters)
        {
            return Ok(await _userService.GetAlbumsOfUserAsync(id, parameters));
        }

        /// <summary>
        /// Returns collection of album DTOs with photos for the user sent the request.
        /// The user Id is specified from the JWT in the Authorize request header.
        /// </summary>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("me/albumsWithPhotos")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetAlbumsWithPhotos([FromQuery] RequestParameters parameters)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value ??
                throw new UserException("The user id claim not found.") { StatusCode = 401 };

            return await GetAlbumsWithPhotos(userId, parameters);
        }

        /// <summary>
        /// Returns collection of album DTOs with photos for the user specified by Id.
        /// </summary>
        /// <param name="parameters">Specifies items per page, page number, and ordering criterion</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{userId:alpha}/albumsWithPhotos")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> GetAlbumsWithPhotos(string userId, [FromQuery] RequestParameters parameters)
        {
            return Ok(await _userService.GetAlbumsWithPhotosOfUserAsync(userId, parameters));
        }
    }
}
