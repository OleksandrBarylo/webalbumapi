﻿using BLL.DataTransferObjects;
using BLL.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    /// <summary>
    /// Common interface for controller HTTP CRUD methods (without "Create").
    /// Supposed for admin uses.
    /// </summary>
    /// <typeparam name="TDto">Type of DTO object.</typeparam>
    /// <typeparam name="TId">Type of entity Id.</typeparam>
    /// <typeparam name="TUpdateDto">Type of DTO to update.</typeparam>
    public interface IPartialAdminCrudController<TDto, TUpdateDto, TId>
        where TDto: IDtoMetadata
        where TUpdateDto: IDtoMetadata
    {
        Task<IActionResult> Get(TId id);
        Task<IActionResult> GetCollection(RequestParameters parameters);
        Task<IActionResult> Update(TId id, TUpdateDto updateDto);
        Task<IActionResult> Delete(TId id); 
    }
}
