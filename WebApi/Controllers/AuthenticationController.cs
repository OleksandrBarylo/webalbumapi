﻿using BLL.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using BLL.DataTransferObjects.UserDtos;
using WebApi.Filters;
using IAuthorizationService = BLL.Services.Interfaces.IAuthorizationService;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthorizationService _authService;
        private readonly ILoggerManager _logger;

        public AuthenticationController(IAuthorizationService authService, ILoggerManager logger)
        {
            _authService = authService;
            _logger = logger;
        }

        /// <summary>
        /// Registration of a new user.
        /// </summary>
        /// <param name="userForRegistration">DTO for a user to register</param>
        /// <returns></returns>
        [HttpPost("register")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> Register([FromBody] UserToRegistrateUpdateDto userForRegistration)
        {
            var result = await _authService.RegisterAsync(userForRegistration);

            if (!result.Succeeded)
            {
                result.Errors.ToList().ForEach(e => ModelState.TryAddModelError(e.Code, e.Description));
                return BadRequest(ModelState);
            }

            return CreatedAtRoute("Login", new { userForAuthentication = 
                new UserToAuthenticateDto { Password = userForRegistration.Password, UserName = userForRegistration.UserName } });
        }

        /// <summary>
        /// Log in the user account. 
        /// </summary>
        /// <param name="userForAuthentication">DTO to log in</param>
        /// <returns></returns>
        [HttpPost("login", Name = "Login")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> Login([FromBody] UserToAuthenticateDto userForAuthentication)
        {
            if (!await _authService.ValidateUserAsync(userForAuthentication))
            {
                _logger.LogWarn("Invalid username or password.");
                return BadRequest();
            }

            return Ok(new { Token = await _authService.CreateTokenAsync() });
        }
    }
}
