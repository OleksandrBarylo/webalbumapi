﻿using Microsoft.AspNetCore.Mvc;

namespace WebApi.Filters
{
    /// <summary>
    /// Provides ability to be used as a stand-alone filter.
    /// </summary>
    public class ValidateModelAttribute : TypeFilterAttribute
    {
        public ValidateModelAttribute() : base(typeof(ValidationFilter)) { }
    }
}
