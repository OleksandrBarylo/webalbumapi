﻿using BLL.DataTransferObjects;
using BLL.Infrastructure;
using BLL.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Text;

namespace WebApi.Filters
{
    /// <summary>
    /// Checks the incoming DTO model for null and validity.
    /// </summary>
    public class ValidationFilter : IActionFilter
    {
        private readonly ILoggerManager _logger;
        public ValidationFilter(ILoggerManager logger) => _logger = logger;

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var action = context.RouteData.Values["action"]; // fetch the current action
            var controller = context.RouteData.Values["controller"]; // fetch the current controller

            var parameter = context.ActionArguments
                .FirstOrDefault(x => x.Value is IDtoMetadata || x.Value is RequestParameters).Value; // fetch the action DTO or request parameters argument

            if (parameter == null)
            {
                string message = $"Object sent from the client is null. Controller: {controller}, action: {action}";

                _logger.LogError(message); // log the error message

                context.Result = new BadRequestObjectResult(message);
            } else if (!context.ModelState.IsValid) // check the model for validity
            {
                StringBuilder messageBuilder = new StringBuilder($"Invalid object model state. Controller: {controller}, action: {action}");
                var errors = context.ModelState.Values.SelectMany(v => v.Errors.Select(e => $"\n{e.ErrorMessage}"));
                foreach (var error in errors)
                    messageBuilder.Append(error);

                _logger.LogError(messageBuilder.ToString()); // log the error message

                context.Result = new UnprocessableEntityObjectResult(context.ModelState); // make short circuit with corresponding result
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // no need
        }
    }
}
