﻿using System.Text;
using BLL.Infrastructure;
using BLL.Infrastructure.Models;
using BLL.Mapper;
using BLL.Services;
using BLL.Services.Interfaces;
using DAL.Context;
using DAL.Entities;
using DAL.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using WebApi.Filters;

namespace WebApi.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            // Logger dependency resolution
            services.AddSingleton<ILoggerManager, LoggerManager>();

            // AutoMapper dependency resolution
            services.AddAutoMapper(config => config.AddProfile(new MappingProfile()));

            // Db context dependency resolution
            services.AddDbContext<ApplicationContext>(options => 
                options.UseSqlServer(configuration.GetConnectionString("PhotoAlbum")));

            // Repositories dependencies resolution
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Services dependencies resolution
            services.AddScoped<IAuthorizationService, AuthorizationService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPhotoService, PhotoService>();
            services.AddScoped<IRatingService, RatingService>();
            services.AddScoped<IAlbumService, AlbumService>();

            // Identity dependencies resolution 
            services.AddTransient<UserManager<User>>();

            // Filter dependencies resolution
            services.AddScoped<ValidationFilter>();
        }

        /// <summary>
        /// Swagger configuration with UI and JWT bearer support.
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "My API",
                    Version = "v1"
                });
                // provides ability to authorize and add the Authorization header with JWT to each consequtive request
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                   {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                  });
            });
        }

        /// <summary>
        /// Configures identity user and role settings.
        /// </summary>
        /// <param name="services"></param>
        public static void AddConfiguredIdentity(this IServiceCollection services)
        {
            var builder = services.AddIdentityCore<User>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequiredLength = 6;
                options.User.RequireUniqueEmail = true;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);

            builder.AddEntityFrameworkStores<ApplicationContext>()
                .AddDefaultTokenProviders();
        }

        /// <summary>
        /// Configures JSON Web Token and declares how to validate it.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddConfiguredJwt(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtOptions = new JwtConfigurationOptions(configuration); // jwt options e.g. valid issuer, valid audience, expiration time
            var secretKey = configuration.GetSection("AppSettings").GetSection("secret").Value; // the secret key to verify the token

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,

                ValidIssuer = jwtOptions.ValidIssuer,
                ValidAudience = jwtOptions.ValidAudience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey))
            });
        }
    }
}
