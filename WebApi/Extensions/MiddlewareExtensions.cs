﻿using System.Net;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Infrastructure.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace WebApi.Extensions
{
    public static class MiddlewareExtensions
    {
        /// <summary>
        /// Configures global exception handler. Sends status code that was specified or 500 as default and object describing error.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="logger"></param>
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILoggerManager logger)
        {
            app.UseExceptionHandler(app => app.Run(async context =>
            {
                context.Response.ContentType = "application/json";

                var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                if (contextFeature != null) 
                {
                    if (contextFeature.Error is IHttpException ex) // if the exception is custom, it refers mostly to client issues
                    {
                        context.Response.StatusCode = ex.StatusCode != default ? ex.StatusCode : (int)HttpStatusCode.NotFound;
                        await context.Response.WriteAsync(new ErrorDetails
                        {
                            Message = contextFeature.Error.Message,
                            StatusCode = context.Response.StatusCode
                        }.ToString());

                        logger.LogInfo($"Request info: {ex.Message}. Status code: {ex.StatusCode}.");
                    } else // else if there was an internal server error...
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        await context.Response.WriteAsync(new ErrorDetails
                        {
                            Message = "Internal Server Error",
                            StatusCode = context.Response.StatusCode
                        }.ToString());

                        logger.LogError($"Something went wrong: {contextFeature.Error}");
                    }
                }
            }));
        }
    }
}
