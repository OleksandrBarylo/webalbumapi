﻿using AutoMapper;
using BLL.DataTransferObjects.AlbumDtos;
using BLL.DataTransferObjects.PhotoDtos;
using DAL.Entities;
using System.Linq;
using BLL.DataTransferObjects.RatingDtos;
using BLL.DataTransferObjects.UserDtos;

namespace BLL.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Photo, PhotoDto>()
                .ForMember(dest => dest.AverageRate, opt => opt.MapFrom(src => src.Ratings.Select(r => r.Rate).DefaultIfEmpty(0).Average()))
                .ForMember(dest => dest.RatesCount, opt => opt.MapFrom(src => src.Ratings != null ? src.Ratings.Count : 0))
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(src => src.Album.AuthorId)) // changed
                .ReverseMap();

            CreateMap<PhotoToAddDto, Photo>()
                .ForMember(dest => dest.FileName, opt => opt.MapFrom(src => "tmp"));

            CreateMap<PhotoToUpdateDto, Photo>();

            CreateMap<Rating, RatingDto>()
                .ReverseMap();

            CreateMap<RatingToAddUpdateDto, Rating>();

            CreateMap<Album, AlbumWithPhotosDto>()
                .ForMember(dest => dest.PhotosNum, opt => opt.MapFrom(src => src.Photos != null ? src.Photos.Count : 0))
                .ForMember(dest => dest.Photos, opt => opt.MapFrom(src => src.Photos))
                .PreserveReferences()
                .ReverseMap();

            CreateMap<Album, AlbumDto>()
                .ForMember(dest => dest.PhotosNum, opt => opt.MapFrom(src => src.Photos != null ? src.Photos.Count : 0))
                .ReverseMap();

            CreateMap<AlbumToAddUpdateDto, Album>();

            CreateMap<UserToRegistrateUpdateDto, User>();

            CreateMap<UserToAuthenticateDto, User>();

            CreateMap<User, UserDto>()
                .ForMember(dest => dest.PhotosPosted, opt => opt.MapFrom(src => src.Albums != null ? src.Albums.Sum(a => a.Photos != null ? a.Photos.Count : 0) : 0))
                .ForMember(dest => dest.AlbumsCreated, opt => opt.MapFrom(src => src.Albums != null ? src.Albums.Count : 0))
                .PreserveReferences()
                .ReverseMap();
        }
    }
}
