﻿using BLL.DataTransferObjects;
using BLL.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    /// <summary>
    /// Interface defining the CRUD operations (without "Create") over the domain objects for their services.
    /// The create operation is too specific to be defined in a generalized interface.
    /// </summary>
    /// <typeparam name="TDto">DTO type representing the complete state of an entity.</typeparam>
    /// <typeparam name="TCreateDto">DTO type to add a new entity.</typeparam>
    /// <typeparam name="TUpdateDto">DTO type to update an existing entity.</typeparam>
    /// <typeparam name="TId">Id type of the dependent entity.</typeparam>
    /// <typeparam name="TMainId">Id type of the entity that is the main (aggregating) entity. Is used to add new dependent entities.</typeparam>
    public interface IPartialCrudService<TDto, TUpdateDto, TId>
        where TDto: IDtoMetadata
        where TUpdateDto: IDtoMetadata
    {
        /// <summary>
        /// Return a single DTO.
        /// </summary>
        /// <param name="id">DTO id</param>
        /// <returns></returns>
        Task<TDto> GetAsync(TId id);
        /// <summary>
        /// Return a collection of DTOs.
        /// </summary>
        /// <param name="parameters">Specifies the page size, page number, ordering criteria</param>
        /// <returns></returns>
        Task<IEnumerable<TDto>> GetCollectionAsync(RequestParameters parameters);
        /// <summary>
        /// Updates a single entity using data sent by the DTO for update.
        /// </summary>
        /// <param name="id">Id of entity to update</param>
        /// <param name="updateDto">DTO with updated information</param>
        /// <returns></returns>
        Task<TDto> UpdateAsync(TId id, TUpdateDto updateDto);
        /// <summary>
        /// Deletes a single entity by Id.
        /// </summary>
        /// <param name="id">Id of entity to delete</param>
        /// <returns></returns>
        Task<TDto> DeleteAsync(TId id);
    }
}
