﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using BLL.DataTransferObjects.UserDtos;

namespace BLL.Services.Interfaces
{
    public interface IAuthorizationService
    {
        Task<IdentityResult> RegisterAsync(UserToRegistrateUpdateDto userForRegistration);
        Task<bool> ValidateUserAsync(UserToAuthenticateDto userForAuthentication);
        Task<string> CreateTokenAsync();
    }
}
