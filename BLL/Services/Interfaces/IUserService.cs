﻿using BLL.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DataTransferObjects.AlbumDtos;
using BLL.DataTransferObjects.PhotoDtos;
using BLL.DataTransferObjects.RatingDtos;
using BLL.DataTransferObjects.UserDtos;

namespace BLL.Services.Interfaces
{
    public interface IUserService : IPartialCrudService<UserDto, UserToRegistrateUpdateDto, string>
    {
        Task<IEnumerable<PhotoDto>> GetPhotosByAuthorAsync(string authorId, RequestParameters parameters);
        Task<AlbumDto> GetAlbumOfUserAsync(string authorId, int albumId);
        Task<AlbumWithPhotosDto> GetAlbumWithPhotosOfUserAsync(string authorId, int albumId);
        Task<IEnumerable<RatingDto>> GetRatingsLeftByUserAsync(string userId, RequestParameters parameters);
        Task<IEnumerable<PhotoDto>> GetMostRatedPhotosAsync(string authorId, int num);
        Task<IEnumerable<PhotoDto>> GetPhotosWithHighestRateAsync(string authorId, int num);
        Task<IEnumerable<AlbumDto>> GetAlbumsOfUserAsync(string userId, RequestParameters parameters);
        Task<IEnumerable<AlbumWithPhotosDto>> GetAlbumsWithPhotosOfUserAsync(string authorId, RequestParameters parameters);
        Task<RatingDto> DeleteRatingLeftByUserAsync(string rateAuthorId, int ratingId);
        Task<RatingDto> UpdateRatingLeftByUserAsync(string rateAuthorId, int ratingId, RatingToAddUpdateDto ratingToUpdate);
    }
}
