﻿using BLL.DataTransferObjects.PhotoDtos;
using BLL.Infrastructure.Models;
using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DataTransferObjects.RatingDtos;

namespace BLL.Services.Interfaces
{
    public interface IPhotoService : IPartialCrudService<PhotoDto, PhotoToUpdateDto, int>
    {
        Task<PhotoDto> AddPhotoToAuthorAlbumAsync(string authorId, int albumId, PhotoToAddDto photoToAdd);
        Task<PhotoDto> UpdateAuthoredPhotoAsync(string authorId, int photoId, PhotoToUpdateDto photoToUpdate);
        Task<PhotoDto> RemovePhotoFromAuthorAlbumAsync(string authorId, int albumId, int photoId);
        Task<IEnumerable<RatingDto>> GetRatingsForPhotoAsync(int photoId, RequestParameters parameters);
        Task<RatingDto> GetRatingForPhotoAsync(int photoId, int ratingId);
        Task<Photo> ResolvePhotoAdditionAsync(PhotoToAddDto photoToAdd, Album album);
        Task<PhotoDto> ResolvePhotoDeletionAsync(Photo photoToDelete);
        Task<PhotoDto> ResolvePhotoToDtoConvert(Photo photo);
    }
}

#region Backup
//Task<IEnumerable<PhotoDto>> GetPhotosByAlbumAsync(int albumId, RequestParameters parameters);
//Task<IEnumerable<PhotoDto>> GetPhotosByAuthorAsync(string authorId, RequestParameters parameters);
//Task<IEnumerable<PhotoDto>> GetMostRatedPhotosAsync(string authorId, int num);
//Task<IEnumerable<PhotoDto>> GetPhotosWithHighestRateAsync(string authorId, int num);
#endregion
