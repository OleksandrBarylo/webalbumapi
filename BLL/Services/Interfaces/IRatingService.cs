﻿using System.Threading.Tasks;
using BLL.DataTransferObjects.RatingDtos;

namespace BLL.Services.Interfaces
{
    public interface IRatingService : IPartialCrudService<RatingDto, RatingToAddUpdateDto, int>
    {
        Task<RatingDto> AddRatingAsync(string rateAuthorId, int photoId, RatingToAddUpdateDto ratingToAdd);
        Task<RatingDto> DeleteRatingOfUserAsync(string rateAuthorId, int ratingId);
        Task<RatingDto> UpdateRatingOfUserAsync(string rateAuthorId, int ratingId, RatingToAddUpdateDto ratingToUpdate);
    }
}