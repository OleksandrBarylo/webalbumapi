﻿using BLL.DataTransferObjects.AlbumDtos;
using BLL.Infrastructure.Models;
using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DataTransferObjects.PhotoDtos;

namespace BLL.Services.Interfaces
{
    public interface IAlbumService : IPartialCrudService<AlbumDto, AlbumToAddUpdateDto, int>
    {
        Task<AlbumWithPhotosDto> GetWithPhotosAsync(int id);
        Task<AlbumDto> AddAlbumAsync(string authorId, AlbumToAddUpdateDto albumToAdd);
        Task<AlbumDto> UpdateAlbumOfUserAsync(string userId, int albumId, AlbumToAddUpdateDto albumToUpdate);
        Task<AlbumDto> DeleteAlbumAsync(string userId, int albumId);
        Task<IEnumerable<PhotoDto>> GetPhotosByAlbumAsync(int albumId, RequestParameters parameters);
        Task<AlbumWithPhotosDto> ResolveAlbumToAlbumWithPhotosDtoAsync(Album album);
        Task<AlbumDto> ResolveAlbumDeletionAsync(Album album);
    }
}
