﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DataTransferObjects.AlbumDtos;
using BLL.DataTransferObjects.PhotoDtos;
using BLL.DataTransferObjects.RatingDtos;
using BLL.DataTransferObjects.UserDtos;

namespace BLL.Services
{
    /// <summary>
    /// Business logic for users.
    /// </summary>
    public class UserService : ServiceBase, IUserService
    {
        private readonly IAlbumService _albumService;
        private readonly IPhotoService _photoService;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper, IAlbumService albumService, IPhotoService photoService) : base(unitOfWork, mapper) 
        {
            _albumService = albumService;
            _photoService = photoService;
        }

        /// <summary>
        /// Returns the user by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<UserDto> GetAsync(string id)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(id) ??
               throw new UserException(id) { StatusCode = 404 };

            return _mapper.Map<UserDto>(user);
        }

        /// <summary>
        /// Returns the collection of user DTOs.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserDto>> GetCollectionAsync(RequestParameters parameters)
        {
            var users = await _uow.UserRepository.GetAll(trackChanges: true)
                .OrderByParameters<User, string>(parameters)
                .PaginateByParameters<User, string>(parameters)
                .Select(u => _mapper.Map<UserDto>(u))
                .ToListAsync();

            return users;
        }

        /// <summary>
        /// Updates a user specified by the id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        public async Task<UserDto> UpdateAsync(string id, UserToRegistrateUpdateDto updateDto)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(id) ??
                throw new UserException(id);

            _mapper.Map(updateDto, user);

            await _uow.SaveChangesAsync();

            return _mapper.Map<UserDto>(user);
        }

        /// <summary>
        /// Deletes a user specified by the id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<UserDto> DeleteAsync(string id)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(id) ??
                throw new UserException(id);

            _uow.UserRepository.Delete(user);

            await _uow.SaveChangesAsync();

            return _mapper.Map<UserDto>(user);
        }

        /// <summary>
        /// Returns photos belonging to a user according to request parameters. 
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PhotoDto>> GetPhotosByAuthorAsync(string authorId, RequestParameters parameters)
        {
            var authoredPhotos = await _uow.PhotoRepository.FindPhotosByAuthor(authorId, trackChanges: false)
                .OrderByParameters<Photo, int>(parameters)
                .PaginateByParameters<Photo, int>(parameters)
                .ToListAsync();

            var photoDtos = new List<PhotoDto>();
            foreach (var p in authoredPhotos)
                photoDtos.Add(await _photoService.ResolvePhotoToDtoConvert(p));

            return _mapper.Map<IEnumerable<PhotoDto>>(photoDtos);
        }

        /// <summary>
        /// Returns album DTO specified by Id for a user specified by Id if such exist and the album belongs to the user.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="albumId"></param>
        /// <returns></returns>
        public async Task<AlbumDto> GetAlbumOfUserAsync(string authorId, int albumId)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(authorId) ?? throw new UserException(authorId) { StatusCode = 404 };

            var album = await _uow.AlbumRepository.FindAlbumForUserAsync(user.Id, albumId, trackChanges: false) ??
                throw new AlbumException($"Album with id {albumId} not found for user with id {authorId}") { StatusCode = 404 };

            return _mapper.Map<AlbumDto>(album);
        }

        /// <summary>
        /// Returns the album with photos specified by Id of a specified by Id user if such exist and the album belongs to the user.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="albumId"></param>
        /// <returns></returns>
        public async Task<AlbumWithPhotosDto> GetAlbumWithPhotosOfUserAsync(string authorId, int albumId)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(authorId) ?? throw new UserException(authorId) { StatusCode = 404 };

            var album = await _uow.AlbumRepository.FindAlbumForUserAsync(user.Id, albumId, trackChanges: false) ??
                throw new AlbumException($"Album with id {albumId} not found for user with id {authorId}") { StatusCode = 404 };

            return await _albumService.ResolveAlbumToAlbumWithPhotosDtoAsync(album);
        }

        /// <summary>
        /// Returns all ratings left by a user specified by a userId.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<RatingDto>> GetRatingsLeftByUserAsync(string userId, RequestParameters parameters)
        {
            if (await _uow.UserRepository.FindUserByIdAsync(userId, trackChanges: false) == null)
                throw new UserException(userId) { StatusCode = 404 };

            var ratings = await _uow.RatingRepository.FindAllRatingsLeftByUser(userId, trackChanges: false)
                .OrderByParameters<Rating, int>(parameters)
                .PaginateByParameters<Rating, int>(parameters)
                .ToListAsync();

            return _mapper.Map<IEnumerable<RatingDto>>(ratings);
        }

        /// <summary>
        /// Returns all albums without photos of a specified user or throws UserException with status code 404 is the user wasn't found.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AlbumDto>> GetAlbumsOfUserAsync(string userId, RequestParameters parameters)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(userId) ?? throw new UserException(userId) { StatusCode = 404 };

            var albums = await _uow.AlbumRepository.FindAlbumsByUser(user.Id, trackChanges: true)
                .OrderByParameters<Album, int>(parameters)
                .PaginateByParameters<Album, int>(parameters)
                .ToListAsync();

            return await Task.Run(() => _mapper.Map<IEnumerable<AlbumDto>>(albums));
        }

        /// <summary>
        /// Returns all albums with photos of a specified user or throws UserException with status code 404 is the user wasn't found.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AlbumWithPhotosDto>> GetAlbumsWithPhotosOfUserAsync(string authorId, RequestParameters parameters)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(authorId) ??
                throw new UserException(authorId) { StatusCode = 404 };

            var albums = await _uow.AlbumRepository.FindAlbumsByUser(user.Id, trackChanges: true)
                .OrderByParameters<Album, int>(parameters)
                .PaginateByParameters<Album, int>(parameters)
                .ToListAsync();

            var albumDtos = new List<AlbumWithPhotosDto>();

            foreach (var a in albums)
                albumDtos.Add(await _albumService.ResolveAlbumToAlbumWithPhotosDtoAsync(a));

            return albumDtos;
        }

        /// <summary>
        /// Returns num photos of author with highest rates.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PhotoDto>> GetPhotosWithHighestRateAsync(string authorId, int num)
        {
            var author = await _uow.UserRepository.GetByCondition(u => u.Id.Equals(authorId), trackChanges: false).FirstOrDefaultAsync() ??
                throw new UserException(authorId) { StatusCode = 404 };

            var photos = await _uow.PhotoRepository.GetAll(trackChanges: true)
                .Where(p => p.Album.AuthorId.Equals(author.Id) && p.Ratings != null)
                .OrderByDescending(p => p.Ratings.Average(r => r.Rate))
                .Take(num)
                .ToListAsync();

            var photoDtos = new List<PhotoDto>();
            foreach (var p in photos.ToArray())
                photoDtos.Add(await _photoService.ResolvePhotoToDtoConvert(p));

            return photoDtos;
        }

        /// <summary>
        /// Returns num photos of author with the biggest number of rates.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PhotoDto>> GetMostRatedPhotosAsync(string authorId, int num)
        {
            var author = await _uow.UserRepository.GetByCondition(u => u.Id.Equals(authorId), trackChanges: false).FirstOrDefaultAsync() ??
                throw new UserException(authorId) { StatusCode = 404 };

            var photos = await _uow.PhotoRepository.GetAll(trackChanges: true)
                .Where(p => p.Album.AuthorId.Equals(author.Id) && p.Ratings != null)
                .OrderByDescending(p => p.Ratings.Count)
                .Take(num)
                .ToListAsync();

            var photoDtos = new List<PhotoDto>();
            foreach (var p in photos.ToArray())
                photoDtos.Add(await _photoService.ResolvePhotoToDtoConvert(p));

            return photoDtos;
        }

        /// <summary>
        /// Updates a rating of a specific user.
        /// </summary>
        /// <param name="rateAuthorId"></param>
        /// <param name="ratingId"></param>
        /// <param name="ratingToUpdate"></param>
        /// <returns></returns>
        public async Task<RatingDto> UpdateRatingLeftByUserAsync(string rateAuthorId, int ratingId, RatingToAddUpdateDto ratingToUpdate)
        {
            if (await _uow.UserRepository.FindUserByIdAsync(rateAuthorId, trackChanges: false) == null)
                throw new UserException(rateAuthorId) { StatusCode = 404 };

            var rating = await _uow.RatingRepository.FindRatingForAuthorAsync(rateAuthorId, ratingId, trackChanges: true);

            _mapper.Map(ratingToUpdate, rating);

            await _uow.SaveChangesAsync();

            return _mapper.Map<RatingDto>(rating);
        }

        /// <summary>
        /// Deletes a rating of a specific user.
        /// </summary>
        /// <param name="rateAuthorId"></param>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        public async Task<RatingDto> DeleteRatingLeftByUserAsync(string rateAuthorId, int ratingId)
        {
            if (await _uow.UserRepository.FindUserByIdAsync(rateAuthorId, trackChanges: false) == null)
                throw new UserException(rateAuthorId) { StatusCode = 404 };

            var rating = await _uow.RatingRepository.GetByIdAsync(ratingId) ??
                throw new RatingException(ratingId) { StatusCode = 404 };

            return _mapper.Map<RatingDto>(rating);
        }
    }
}
