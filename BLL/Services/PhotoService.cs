﻿using AutoMapper;
using BLL.DataTransferObjects.PhotoDtos;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DataTransferObjects.RatingDtos;

namespace BLL.Services
{
    /// <summary>
    /// Business logic for photos.
    /// </summary>
    public class PhotoService : ServiceBase, IPhotoService
    {
        public PhotoService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        /// <summary>
        /// Retuns photo dto by its id.
        /// </summary>
        /// <param name="photoId"></param>
        /// <returns></returns>
        public async Task<PhotoDto> GetAsync(int id)
        {
            var photo = await _uow.PhotoRepository.GetByIdAsync(id) ?? 
                throw new PhotoException(id) { StatusCode = 404 };

            var photoDto = await ResolvePhotoToDtoConvert(photo);

            return photoDto;
        }

        /// <summary>
        /// Returns the collection of photo dtos.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PhotoDto>> GetCollectionAsync(RequestParameters parameters)
        {
            var photos = await _uow.PhotoRepository.GetAll(trackChanges: true)
                .PaginateByParameters<Photo, int>(parameters)
                .OrderByParameters<Photo, int>(parameters)
                .ToListAsync();

            var photoDtos = new List<PhotoDto>(photos.Count);
            foreach (var p in photos)
                photoDtos.Add(await ResolvePhotoToDtoConvert(p));

            return photoDtos;
        }

        /// <summary>
        /// Updates the photo with the specified Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        public async Task<PhotoDto> UpdateAsync(int id, PhotoToUpdateDto updateDto)
        {
            var photo = await _uow.PhotoRepository.GetByIdAsync(id) ??
               throw new PhotoException(id);

            _mapper.Map(updateDto, photo);

            await _uow.SaveChangesAsync();

            var photoDto = await ResolvePhotoToDtoConvert(photo);

            return photoDto;
        }

        /// <summary>
        /// Deletes the photo with the specified Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<PhotoDto> DeleteAsync(int id)
        {
            var photoToDelete = await _uow.PhotoRepository.GetByIdAsync(id) ??
                throw new PhotoException(id);

            var deletedDto = await ResolvePhotoDeletionAsync(photoToDelete);

            return deletedDto;
        }

        /// <summary>
        /// Adds a photo to db.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="albumId"></param>
        /// <param name="photoToAdd"></param>
        /// <returns></returns>
        public async Task<PhotoDto> AddPhotoToAuthorAlbumAsync(string authorId, int albumId, PhotoToAddDto photoToAdd)
        {
            var author = await _uow.UserRepository.GetByCondition(u => u.Id.Equals(authorId), trackChanges: true).FirstOrDefaultAsync() ??
                throw new UserException(authorId) { StatusCode = 404 };

            if (!author.Albums.Any(a => a.Id == albumId))
                throw new AlbumException($"User with id {authorId} doesn't have album with id {albumId}.");

            var album = await _uow.AlbumRepository.GetByCondition(a => a.Id == albumId, trackChanges: true).FirstOrDefaultAsync() ??
                throw new AlbumException(albumId);

            var photo = await ResolvePhotoAdditionAsync(photoToAdd, album);

            var photoDto = _mapper.Map<PhotoDto>(photo);

            photoDto.Base64 = photoToAdd.Base64;

            return photoDto;
        }

        /// <summary>
        /// Deletes specific photo from the db.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="albumId"></param>
        /// <param name="photoId"></param>
        /// <returns></returns>
        public async Task<PhotoDto> RemovePhotoFromAuthorAlbumAsync(string authorId, int albumId, int photoId)
        {
            var author = await _uow.UserRepository.GetByCondition(u => u.Id.Equals(authorId), trackChanges: false).FirstOrDefaultAsync() ??
                throw new UserException(authorId) { StatusCode = 404 };

            if (!author.Albums.Any(a => a.Id == albumId))
                throw new AlbumException($"User with id {authorId} doesn't have album with id {albumId}.");

            var photo = await _uow.PhotoRepository.FindPhoto(authorId, albumId, photoId, trackChanges: false) ??
                throw new PhotoException(photoId);

            _uow.PhotoRepository.Delete(photo);

            await _uow.FileRepository.DeleteFileAsync(photo?.FileName);

            return _mapper.Map<PhotoDto>(photo);
        }

        /// <summary>
        /// Updates specific foto in the db.
        /// </summary>
        /// <param name="authorId"></param>
        /// <param name="albumId"></param>
        /// <param name="photoId"></param>
        /// <param name="photoToUpdate"></param>
        /// <returns></returns>
        public async Task<PhotoDto> UpdateAuthoredPhotoAsync(string authorId, int photoId, PhotoToUpdateDto photoToUpdate)
        {
            var photo = await _uow.PhotoRepository.GetByIdAsync(photoId) ??
                throw new PhotoException(photoId);

            if (!photo.Album.AuthorId.Equals(authorId))
                throw new UserException($"User with Id {authorId} isn't author of photo with Id {photoId}.");

            _mapper.Map(photoToUpdate, photo);

            await _uow.SaveChangesAsync();

            var photoDto = await ResolvePhotoToDtoConvert(photo);

            return photoDto;
        }

        /// <summary>
        /// Returns the collection of ratings for a specific photo.
        /// </summary>
        /// <param name="photoId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<RatingDto>> GetRatingsForPhotoAsync(int photoId, RequestParameters parameters)
        {
            if (await _uow.PhotoRepository.GetByIdAsync(photoId) == null)
                throw new PhotoException(photoId) { StatusCode = 404 };

            var ratings = await _uow.RatingRepository.FindAllRatingsForPhoto(photoId, trackChanges: false)
                .OrderByParameters<Rating, int>(parameters)
                .PaginateByParameters<Rating, int>(parameters)
                .ToListAsync();

            return _mapper.Map<IEnumerable<RatingDto>>(ratings);
        }

        /// <summary>
        /// Returns the rating dto for a specific rating of a specific photo.
        /// </summary>
        /// <param name="photoId"></param>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        public async Task<RatingDto> GetRatingForPhotoAsync(int photoId, int ratingId)
        {
            if (await _uow.PhotoRepository.GetByIdAsync(photoId) == null)
                throw new PhotoException(photoId) { StatusCode = 404 };

            var rating = await _uow.RatingRepository.FindRatingForPhotoAsync(photoId, ratingId, trackChanges: false) ??
                throw new RatingException(ratingId) { StatusCode = 404 };

            return _mapper.Map<RatingDto>(rating);
        }

        /// <summary>
        /// Carries out common logic to convert a photo entity to a photo dto.
        /// </summary>
        /// <param name="photo"></param>
        /// <returns></returns>
        public async Task<PhotoDto> ResolvePhotoToDtoConvert(Photo photo)
        {
            var dto = _mapper.Map<PhotoDto>(photo);
            var base64 = await _uow.FileRepository.GetBase64Async(photo.FileName);

            if (base64 != null)
                dto.Base64 = base64;
            else
            {
                _uow.PhotoRepository.Delete(photo);
                await _uow.SaveChangesAsync();
            }

            return dto;
        }

        /// <summary>
        /// Carries out common logic to add a new photo to the db.
        /// </summary>
        /// <param name="photoToAdd"></param>
        /// <param name="album"></param>
        /// <returns></returns>
        public async Task<Photo> ResolvePhotoAdditionAsync(PhotoToAddDto photoToAdd, Album album)
        {
            var base64 = photoToAdd.Base64.Split(',').Last();

            var photo = _mapper.Map<Photo>(photoToAdd);

            photo.Album = album;

            string fileName = $"{Guid.NewGuid()}.{GetFileExtension(base64)}"; // file name is generated as an Guid with the resolved extension

            photo.FileName = fileName;

            await _uow.PhotoRepository.AddAsync(photo);

            await _uow.FileRepository.SaveFileAsync(fileName, base64);

            await _uow.SaveChangesAsync();

            return photo;
        }

        /// <summary>
        /// Carries out common logic to delete the photo db record and the file in the db.
        /// </summary>
        /// <param name="photoToDelete"></param>
        /// <returns></returns>
        public async Task<PhotoDto> ResolvePhotoDeletionAsync(Photo photoToDelete)
        {
            var deletedDto = await ResolvePhotoToDtoConvert(photoToDelete);

            _uow.PhotoRepository.Delete(photoToDelete);

            await _uow.SaveChangesAsync();

            await _uow.FileRepository.DeleteFileAsync(photoToDelete.FileName);

            return deletedDto;
        }

        /// <summary>
        /// Determines the file extension based on the base64 representation.
        /// </summary>
        /// <param name="base64"></param>
        /// <returns></returns>
        private static string GetFileExtension(string base64)
        {
            var data = base64.Substring(0, 5);

            switch (data.ToUpper())
            {
                case "IVBOR":
                    return "png";
                case "/9J/4":
                    return "jpg";
                case "AAAAF":
                    return "mp4";
                case "JVBER":
                    return "pdf";
                case "AAABA":
                    return "ico";
                case "UMFYI":
                    return "rar";
                case "E1XYD":
                    return "rtf";
                case "U1PKC":
                    return "txt";
                case "MQOWM":
                case "77U/M":
                    return "srt";
                default:
                    return string.Empty;
            }
        }
    }
}
