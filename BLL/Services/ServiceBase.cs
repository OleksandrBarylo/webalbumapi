﻿using AutoMapper;
using DAL.UnitOfWork;

namespace BLL.Services
{
    /// <summary>
    /// Base for business logic classes.
    /// </summary>
    public class ServiceBase
    {
        protected IUnitOfWork _uow;
        protected IMapper _mapper;

        protected ServiceBase(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _uow = unitOfWork;
            _mapper = mapper;
        }
    }
}
