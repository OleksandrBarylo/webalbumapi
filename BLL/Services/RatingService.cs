﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DataTransferObjects.RatingDtos;

namespace BLL.Services
{
    /// <summary>
    /// Business logic for ratings.
    /// </summary>
    public class RatingService : ServiceBase, IRatingService
    {
        public RatingService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }   

        /// <summary>
        /// Returns a rating by index.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<RatingDto> GetAsync(int id)
        {
            var rating = await _uow.RatingRepository.GetByIdAsync(id) ??
                throw new RatingException(id);

            return _mapper.Map<RatingDto>(rating);
        }

        /// <summary>
        /// Returns the collection of ratings.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<RatingDto>> GetCollectionAsync(RequestParameters parameters)
        {
            var ratings = await _uow.RatingRepository.GetAll(trackChanges: true)
                .PaginateByParameters<Rating, int>(parameters)
                .OrderByParameters<Rating, int>(parameters)
                .ToListAsync();

            return _mapper.Map<IEnumerable<RatingDto>>(ratings);
        }

        /// <summary>
        /// Updates a rating.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        public async Task<RatingDto> UpdateAsync(int id, RatingToAddUpdateDto updateDto)
        {
            var ratingToUpdate = await _uow.RatingRepository.GetByIdAsync(id) ??
                throw new RatingException(id);

            _mapper.Map(updateDto, ratingToUpdate);

            await _uow.SaveChangesAsync();

            return _mapper.Map<RatingDto>(ratingToUpdate);
        }

        /// <summary>
        /// Deletes a rating.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<RatingDto> DeleteAsync(int id)
        {
            var ratingToDelete = await _uow.RatingRepository.GetByIdAsync(id) ??
                throw new RatingException(id);

            _uow.RatingRepository.Delete(ratingToDelete);

            await _uow.SaveChangesAsync();

            return _mapper.Map<RatingDto>(ratingToDelete);
        }

        /// <summary>
        /// Adds rating left by user to a photo.
        /// </summary>
        /// <param name="rateAuthorId"></param>
        /// <param name="photoId"></param>
        /// <param name="ratingToAdd"></param>
        /// <returns></returns>
        public async Task<RatingDto> AddRatingAsync(string rateAuthorId, int photoId, RatingToAddUpdateDto ratingToAdd)
        {
            var author = await _uow.UserRepository.FindUserByIdAsync(rateAuthorId, trackChanges: false) ??
                throw new UserException(rateAuthorId) { StatusCode = 404 };

            var photo = await _uow.PhotoRepository.GetByIdAsync(photoId) ??
                throw new PhotoException(photoId) { StatusCode = 404 };

            if (photo.Album.IsPrivate && !author.Id.Equals(rateAuthorId))
                throw new RatingException("The album the photo to rate belongs to is private.");

            var rating = _mapper.Map<Rating>(ratingToAdd);
            rating.RateAuthorId = author.Id;
            rating.RatedPhotoId = photo.Id;

            await _uow.RatingRepository.AddAsync(rating);

            await _uow.SaveChangesAsync();

            return _mapper.Map<RatingDto>(rating);
        }

        /// <summary>
        /// Deletes a rating by Id which is authored by a user with the given Id.
        /// </summary>
        /// <param name="rateAuthorId"></param>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        public async Task<RatingDto> DeleteRatingOfUserAsync(string rateAuthorId, int ratingId)
        {
            var ratingToDelete = await _uow.RatingRepository.GetByIdAsync(ratingId) ??
                throw new RatingException(ratingId);

            if (!ratingToDelete.RateAuthorId.Equals(rateAuthorId))
                throw new UserException($"The rating with id {ratingId} doesn't belong to user with id {rateAuthorId}.");

            _uow.RatingRepository.Delete(ratingToDelete);

            await _uow.SaveChangesAsync();

            return _mapper.Map<RatingDto>(ratingToDelete);
        }

        /// <summary>
        /// Updates a rating by Id which is authored by a user with the given Id.
        /// </summary>
        /// <param name="rateAuthorId"></param>
        /// <param name="ratingId"></param>
        /// <param name="ratingToUpdate"></param>
        /// <returns></returns>
        public async Task<RatingDto> UpdateRatingOfUserAsync(string rateAuthorId, int ratingId, RatingToAddUpdateDto ratingToUpdate)
        {
            var rating = await _uow.RatingRepository.GetByIdAsync(ratingId) ??
                throw new RatingException(ratingId);

            if (!rating.RateAuthorId.Equals(rateAuthorId))
                throw new UserException($"The rating with id {ratingId} doesn't belong to user with id {rateAuthorId}.");

            _mapper.Map(ratingToUpdate, rating);

            await _uow.SaveChangesAsync();

            return _mapper.Map<RatingDto>(rating);
        }
    }
}
