﻿using AutoMapper;
using BLL.Infrastructure.Models;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BLL.DataTransferObjects.UserDtos;

namespace BLL.Services
{
    /// <summary>
    /// Logic for authorization and registration.
    /// </summary>
    public class AuthorizationService : ServiceBase, IAuthorizationService
    {
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private User _user;
        public AuthorizationService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, UserManager<User> userManager) : base(unitOfWork, mapper)
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        /// <summary>
        /// Registers a new user.
        /// </summary>
        /// <param name="userForRegistration">User DTO for registration</param>
        /// <returns></returns>
        public async Task<IdentityResult> RegisterAsync(UserToRegistrateUpdateDto userForRegistration)
        {
            var user = _mapper.Map<User>(userForRegistration);

            var result = await _userManager.CreateAsync(user, userForRegistration.Password);

            if (result.Succeeded)
                await _userManager.AddToRolesAsync(user, userForRegistration.Roles);

            await _uow.SaveChangesAsync();

            return result;
        }

        /// <summary>
        /// Validates whether the user is registered or not.
        /// </summary>
        /// <param name="userForAuthentication">User DTO for authentication</param>
        /// <returns></returns>
        public async Task<bool> ValidateUserAsync(UserToAuthenticateDto userForAuthentication)
        {
            _user = await _userManager.FindByNameAsync(userForAuthentication.UserName);

            return _user != null && await _userManager.CheckPasswordAsync(_user, userForAuthentication.Password);
        }

        /// <summary>
        /// Creates a Json Web Token for the current user.
        /// </summary>
        /// <returns></returns>
        public async Task<string> CreateTokenAsync()
        {
            var credentials = GetSigningCredentials();
            var claims = await GetClaimsAsync();
            var tokenOptions = GenerateTokenOptions(credentials, claims);

            return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
        }

        /// <summary>
        /// Creates the signing credentials for the current user.
        /// </summary>
        /// <returns></returns>
        private SigningCredentials GetSigningCredentials()
        {
            var key = _configuration.GetSection("AppSettings").GetSection("secret").Value; // fetch the secret key to verify the jwt
            var bytes = Encoding.UTF8.GetBytes(key);
            var secret = new SymmetricSecurityKey(bytes);

            return new SigningCredentials(secret, SecurityAlgorithms.HmacSha256); //HmacSha256
        }

        /// <summary>
        /// Returns the user claims for the current user like role, name, Id.
        /// </summary>
        /// <returns></returns>
        private async Task<IEnumerable<Claim>> GetClaimsAsync()
        {
            return (await _userManager.GetRolesAsync(_user))
                .Select(r => new Claim(ClaimTypes.Role, r))
                .Append(new Claim(ClaimTypes.Name, _user.UserName))
                .Append(new Claim(ClaimTypes.NameIdentifier, _user.Id));
        }

        /// <summary>
        /// Generates the JWT options based on the user credentials and claims.
        /// </summary>
        /// <param name="credentials">User signing credentials</param>
        /// <param name="claims">User claims</param>
        /// <returns></returns>
        private JwtSecurityToken GenerateTokenOptions(SigningCredentials credentials, IEnumerable<Claim> claims)
        {
            // options are fetched from configuration
            var options = new JwtConfigurationOptions(_configuration);

            return new JwtSecurityToken(
                issuer: options.ValidIssuer,
                audience: options.ValidAudience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(options.LifeTime),
                signingCredentials: credentials);
        }
    }
}
