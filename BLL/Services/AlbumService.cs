﻿using AutoMapper;
using BLL.DataTransferObjects.AlbumDtos;
using BLL.Exceptions;
using BLL.Infrastructure.Models;
using BLL.Infrastructure;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DataTransferObjects.PhotoDtos;

namespace BLL.Services
{
    /// <summary>
    /// Business logic for albums.
    /// </summary>
    public class AlbumService : ServiceBase, IAlbumService
    {
        private readonly IPhotoService _photoService;

        public AlbumService(IUnitOfWork unitOfWork, IMapper mapper, IPhotoService photoService) : base(unitOfWork, mapper) =>
            _photoService = photoService;

        /// <summary>
        /// Returns the album DTO by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AlbumDto> GetAsync(int id)
        {
            var album = await _uow.AlbumRepository.GetByIdAsync(id) ??
                throw new AlbumException(id);

            return _mapper.Map<AlbumDto>(album);    
        }

        /// <summary>
        /// Returns the album DTO with photos by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AlbumWithPhotosDto> GetWithPhotosAsync(int id)
        {
            var album = await _uow.AlbumRepository.GetByIdAsync(id) ??
                throw new AlbumException(id);

            return await ResolveAlbumToAlbumWithPhotosDtoAsync(album);
        }

        /// <summary>
        /// Returns the collection of album DTOs.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AlbumDto>> GetCollectionAsync(RequestParameters parameters)
        {
            var albums = await _uow.AlbumRepository.GetAll(trackChanges: true)
                .PaginateByParameters<Album, int>(parameters)
                .OrderByParameters<Album, int>(parameters)
                .ToListAsync();

            return _mapper.Map<IEnumerable<AlbumDto>>(albums);
        }

        /// <summary>
        /// Updates the album with a specific Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        public async Task<AlbumDto> UpdateAsync(int id, AlbumToAddUpdateDto updateDto)
        {
            var album = await _uow.AlbumRepository.GetByIdAsync(id) ??
                throw new AlbumException(id) { StatusCode = 404 };

            _mapper.Map(updateDto, album);

            await _uow.SaveChangesAsync();

            return _mapper.Map<AlbumDto>(album);
        }

        /// <summary>
        /// Updates the album with specific Id and authored by a specific user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="albumId"></param>
        /// <param name="albumToUpdate"></param>
        /// <returns></returns>
        public async Task<AlbumDto> UpdateAlbumOfUserAsync(string userId, int albumId, AlbumToAddUpdateDto albumToUpdate)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(userId, trackChanges: true) ??
                throw new UserException(userId) { StatusCode = 404 };

            var album = await _uow.AlbumRepository.FindAlbumForUserAsync(user.Id, albumId, trackChanges: true) ??
                throw new AlbumException($"Album with id {albumId} not found for user with id {userId}") { StatusCode = 404 };

            _mapper.Map(albumToUpdate, album);

            await _uow.SaveChangesAsync();

            return _mapper.Map<AlbumDto>(album);
        }

        /// <summary>
        /// Deletes the album by Id with all photos. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AlbumDto> DeleteAsync(int id)
        {
            var albumToDelete = await _uow.AlbumRepository.GetByIdAsync(id) ??
                throw new AlbumException(id);

            var deletedDto = await ResolveAlbumDeletionAsync(albumToDelete);

            return deletedDto;
        }

        /// <summary>
        /// Deletes the album by its Id and authored by a specific user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="albumId"></param>
        /// <returns></returns>
        public async Task<AlbumDto> DeleteAlbumAsync(string userId, int albumId)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(userId, trackChanges: true) ??
                throw new UserException(userId) { StatusCode = 404 };

            var album = await _uow.AlbumRepository.FindAlbumForUserAsync(user.Id, albumId, trackChanges: true) ??
                throw new AlbumException($"Album with id {albumId} not found for user with id {userId}") { StatusCode = 404 };

            _uow.AlbumRepository.Delete(album);

            await _uow.SaveChangesAsync();

            return _mapper.Map<AlbumDto>(album);
        }

        /// <summary>
        /// Adds album to the specified user or throws UserException with status code 404 is the user wasn't found.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="albumToAdd"></param>
        /// <returns></returns>
        public async Task<AlbumDto> AddAlbumAsync(string userId, AlbumToAddUpdateDto albumToAdd)
        {
            var user = await _uow.UserRepository.FindUserByIdAsync(userId) ?? throw new UserException(userId) { StatusCode = 404 };

            var album = _mapper.Map<Album>(albumToAdd);

            album.AuthorId = user.Id;

            album.Photos.Clear();

            await _uow.AlbumRepository.AddAsync(album);

            if (albumToAdd.Photos != null)
                foreach (var p in albumToAdd.Photos)
                    await _photoService.ResolvePhotoAdditionAsync(p, album);

            await _uow.SaveChangesAsync();

            return _mapper.Map<AlbumDto>(album);
        } 

        /// <summary>
        /// Returns photos belonging to an album according to request parameters. 
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PhotoDto>> GetPhotosByAlbumAsync(int albumId, RequestParameters parameters)
        {
            var album = await _uow.AlbumRepository.GetByIdAsync(albumId) ??
                throw new AlbumException(albumId) { StatusCode = 404 };

            var photos = await _uow.PhotoRepository.GetByCondition(p => p.AlbumId == album.Id, trackChanges: true)
                .OrderByParameters<Photo, int>(parameters)
                .PaginateByParameters<Photo, int>(parameters)
                .ToListAsync();

            var photoDtos = new List<PhotoDto>();
            foreach (var p in photos)
                photoDtos.Add(await _photoService.ResolvePhotoToDtoConvert(p));

            return photoDtos;
        }

        /// <summary>
        /// Performs common logic to convert the album entity to the album DTO with photo DTOs.
        /// </summary>
        /// <param name="album"></param>
        /// <returns></returns>
        public async Task<AlbumWithPhotosDto> ResolveAlbumToAlbumWithPhotosDtoAsync(Album album)
        {
            var albumDto = _mapper.Map<AlbumWithPhotosDto>(album);
            var photoDtos = new List<PhotoDto>();
            foreach (var p in album.Photos.ToArray())
                photoDtos.Add(await _photoService.ResolvePhotoToDtoConvert(p));

            albumDto.Photos = photoDtos;

            return albumDto;
        }

        /// <summary>
        /// Performs common logic to delete the album with all dependent photos and the photo files.
        /// </summary>
        /// <param name="album"></param>
        /// <returns></returns>
        public async Task<AlbumDto> ResolveAlbumDeletionAsync(Album album)
        {
            foreach (var p in album.Photos)
                await _photoService.ResolvePhotoDeletionAsync(p);

            _uow.AlbumRepository.Delete(album);

            await _uow.SaveChangesAsync();

            return _mapper.Map<AlbumDto>(album);
        }
    }
}

