﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DataTransferObjects.RatingDtos
{
    public class RatingToAddUpdateDto : IDtoMetadata
    {
        [Required(ErrorMessage = "Rate is required.")]
        [Range(1, 5)]
        public int Rate { get; set; }
        public string Comment { get; set; }
    }
}
