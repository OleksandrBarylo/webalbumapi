﻿namespace BLL.DataTransferObjects.RatingDtos
{
    public class RatingDto : IDtoMetadata
    {
        public int Id { get; set; }
        public int Rate { get; set; }
        public string Comment { get; set; }
        public int RatedPhotoId { get; set; }
        public string RateAuthorId { get; set; }
    }
}
