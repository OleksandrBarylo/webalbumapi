﻿namespace BLL.DataTransferObjects.AlbumDtos
{
    public class AlbumDto : IDtoMetadata
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AuthorId { get; set; }
        public int PhotosNum { get; set; }
    }
}
