﻿using System.Collections.Generic;
using BLL.DataTransferObjects.PhotoDtos;

namespace BLL.DataTransferObjects.AlbumDtos
{
    public class AlbumWithPhotosDto : IDtoMetadata
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string AuthorId { get; set; }
        public int PhotosNum { get; set; }
        public ICollection<PhotoDto> Photos { get; set; }
    }
}
