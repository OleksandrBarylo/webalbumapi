﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.DataTransferObjects.PhotoDtos;

namespace BLL.DataTransferObjects.AlbumDtos
{
    public class AlbumToAddUpdateDto : IDtoMetadata
    {
        [Required(ErrorMessage = "Album name is required.")]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public ICollection<PhotoToAddDto> Photos { get; set; }
    }
}
