﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.DataTransferObjects.UserDtos
{
    /// <summary>
    /// Describes user info for registration. Comes from the client request. 
    /// </summary>
    public class UserToRegistrateUpdateDto : IDtoMetadata 
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required(ErrorMessage = "Username is required.")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "Passwords don't match.")]
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public ICollection<string> Roles { get; set; }
    }
}
