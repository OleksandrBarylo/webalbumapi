﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DataTransferObjects.UserDtos
{
    public class UserToAuthenticateDto : IDtoMetadata
    {
        [Required(ErrorMessage = "Username is required.")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }
    }
}
