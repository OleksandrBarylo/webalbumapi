﻿using System.Collections.Generic;
using BLL.DataTransferObjects.AlbumDtos;

namespace BLL.DataTransferObjects.UserDtos
{
    public class UserDto : IDtoMetadata
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int AlbumsCreated { get; set; }
        public int PhotosPosted { get; set; }
        public ICollection<AlbumDto> Albums { get; set; }
    }
}
