﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DataTransferObjects.PhotoDtos
{
    public class PhotoToAddDto : PhotoToUpdateDto
    {
        public DateTime UploadDate { get; set; }
        [Required(ErrorMessage = "Decoded photo is required.")]
        public string Base64 { get; set; }
    }
}
