﻿namespace BLL.DataTransferObjects.PhotoDtos
{
    public class PhotoToUpdateDto : IDtoMetadata
    {
        public string Description { get; set; }
    }
}
