﻿using System;
using System.Collections.Generic;
using BLL.DataTransferObjects.RatingDtos;

namespace BLL.DataTransferObjects.PhotoDtos
{
    public class PhotoDto : IDtoMetadata
    {
        public int Id { get; set; }
        public string Base64 { get; set; }
        public string Description { get; set; }
        public double AverageRate { get; set; }
        public int RatesCount { get; set; }
        public DateTime UploadDate { get; set; }
        public string AuthorId { get; set; }
        public ICollection<RatingDto> Ratings { get; set; }
    }
}
