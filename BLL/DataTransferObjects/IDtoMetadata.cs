﻿namespace BLL.DataTransferObjects
{
    /// <summary>
    /// Used just to mark that an object is of DTO type to validate in filters. 
    /// </summary>
    public interface IDtoMetadata
    {  
    }
}
