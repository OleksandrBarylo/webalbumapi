﻿using System;
using System.Runtime.Serialization;

namespace BLL.Exceptions
{
    [Serializable]
    public class AlbumException : Exception, IHttpException
    {

        public AlbumException(string message) : base(message)
        {
        }

        public AlbumException(int id) : base($"Album with id {id} not found.")
        {
        }

        protected AlbumException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public int StatusCode { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
