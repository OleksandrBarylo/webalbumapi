﻿using System;
using System.Runtime.Serialization;

namespace BLL.Exceptions
{
    [Serializable]
    public class PhotoException : Exception, IHttpException
    {
        public PhotoException(string message) : base(message)
        {
        }

        public PhotoException(int id) : base($"Photo with id {id} not found.")
        {
        }

        protected PhotoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public int StatusCode { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
