﻿using System;
using System.Runtime.Serialization;

namespace BLL.Exceptions
{
    [Serializable]
    public class RatingException : Exception, IHttpException
    {
        public RatingException(string message) : base(message)
        {
        }

        public RatingException(int id) : base($"Rating with id {id} not found.")
        {
        }

        protected RatingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public int StatusCode { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
