﻿using System;
using System.Runtime.Serialization;

namespace BLL.Exceptions
{
    [Serializable]
    public class UserException : Exception, IHttpException
    {
        public UserException(string id) : base($"User with id {id} not found.")
        {
        }

        protected UserException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public int StatusCode { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
