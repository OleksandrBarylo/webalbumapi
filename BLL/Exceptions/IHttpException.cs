﻿namespace BLL.Exceptions
{
    public interface IHttpException
    {
        string Message { get; }
        int StatusCode { get; }
    }
}
