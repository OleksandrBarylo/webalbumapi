﻿using BLL.Infrastructure.Models;
using DAL.Entities;
using System;
using System.Linq;
using System.Reflection;
using System.Linq.Dynamic.Core;

namespace BLL.Infrastructure
{
    public static class RequestParametersResolver
    {
        /// <summary>
        /// Orders queryable entities according to properties specified in the query string.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> OrderByParameters<TEntity, TId>(this IQueryable<TEntity> entities, RequestParameters parameters) 
            where TEntity: IEntity<TId>
        {
            if (!entities.Any()) return entities;

            var orderQueryStr = parameters.OrderBy;

            if (string.IsNullOrEmpty(orderQueryStr)) return entities.OrderBy(e => e.Id);

            var orderParams = orderQueryStr.Trim(' ').Split(',', StringSplitOptions.RemoveEmptyEntries);

            var propInfos = entities.First().GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            var query = string.Join(',', orderParams
                .Select(param => propInfos.FirstOrDefault(p => p.Name.Equals(param.Split(' ').First(), 
                StringComparison.InvariantCultureIgnoreCase))?.Name + (param.EndsWith(" desc") ? " descending" : " ascending"))
                .Where(param => param != null && !param.StartsWith(' ')));

            if (!string.IsNullOrWhiteSpace(query))
                return entities.OrderBy(query);
            else return entities.OrderBy(e => e.Id);
        }

        /// <summary>
        /// Returns entities according to page size and page number specified in the query string.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> PaginateByParameters<TEntity, TId>(this IQueryable<TEntity> entities, RequestParameters parameters) 
            where TEntity : IEntity<TId>
        {
            return entities.Skip((parameters.PageNum - 1) * parameters.PageSize).Take(parameters.PageSize);
        }
    }
}
