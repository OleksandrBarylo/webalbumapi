﻿using Newtonsoft.Json;

namespace BLL.Infrastructure.Models
{
    public class ErrorDetails
    {
        /// <summary>
        /// Error status code.
        /// </summary>
        public int StatusCode { get; set; }
        /// <summary>
        /// Message describing the error.
        /// </summary>
        public string Message { get; set; }
        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
