﻿using Microsoft.Extensions.Configuration;

namespace BLL.Infrastructure.Models
{
    public class JwtConfigurationOptions
    {
        public static string SectionName => "JwtSettings";

        private readonly IConfiguration _configuration;
        public JwtConfigurationOptions(IConfiguration configuration) => _configuration = configuration.GetSection(SectionName);
        public string ValidIssuer => _configuration.GetSection("validIssuer").Value;
        public string ValidAudience => _configuration.GetSection("validAudience").Value;
        public int LifeTime => int.Parse(_configuration.GetSection("expires").Value);
    }
}
