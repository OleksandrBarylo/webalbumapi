﻿using System.ComponentModel.DataAnnotations;

namespace BLL.Infrastructure.Models
{
    /// <summary>
    /// Model class representing inline query parameters to paginate and sort resources to get from an endpoint.
    /// </summary>
    public class RequestParameters
    {
        public const int MaxPageSize = 50;
        private int _pageSize = 10;
        [Range(1, MaxPageSize)]
        public int PageSize { get => _pageSize; set => _pageSize = value > MaxPageSize ? MaxPageSize : value; }
        [Range(1, int.MaxValue)]
        public int PageNum { get; set; } = 1;
        /// <summary>
        /// Example for photos: averageRate desc,description
        /// </summary>
        public string OrderBy { get; set; }
    }
}
