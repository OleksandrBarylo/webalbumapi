﻿using DAL.Entities;
using DAL.UnitOfWork;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLTests
{
    public class TestHelper
    {
        public Mock<IUnitOfWork> UowPlainMock => new Mock<IUnitOfWork>();

        public List<User> Users = new List<User>
        {
            new User { FirstName = "Alex", LastName = "Subborn", Id = "08962dfe-7f12-4507-b5f8-1fcd83246cf2"  }
        }.Select(x => { x.Id = Guid.NewGuid().ToString(); return x; }).ToList();

        public readonly List<Album> Albums = new List<Album>
        {
            new Album { AuthorId = "08962dfe-7f12-4507-b5f8-1fcd83246cf2", Name = "album1", Description = "album1Description" },
            //new Album { AuthorId}
        }.Select((x, i) => { x.Id = i + 1; return x; }).ToList();

        public readonly List<Photo> Photos = new List<Photo>
        {
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
            new Photo { Description = "photo2", FileName = "file2", UploadDate = DateTime.Now },
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
            new Photo { Description = "photo1", FileName = "file1", UploadDate = DateTime.Now },
        }.Select((x, i) => { x.Id = i + 1; return x; }).ToList();
    }
}
