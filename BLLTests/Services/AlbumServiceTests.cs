﻿using Xunit;
using BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using BLLTests;
using Moq;
using AutoMapper;

namespace BLL.Services.Tests
{
    public class AlbumServiceTests
    {
        private TestHelper Helper => new TestHelper();
        private IMapper Mapper => null;

        [Fact()]
        public void AlbumServiceTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        //[Theory]
        //[ClassData(,new Album { })]
        public void GetAsyncTest()
        {
            var album = new Album { Id = 5 };
            var uowMock = Helper.UowPlainMock;
            uowMock.SetupGet(x => x.AlbumRepository.GetByIdAsync(It.IsAny<int>()));
            var service = new AlbumService(uowMock.Object, null, null); //TODO: complete this

        }

        [Fact()]
        public void GetWithPhotosAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void GetCollectionAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void UpdateAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void UpdateAlbumOfUserAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void DeleteAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void DeleteAlbumAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void AddAlbumAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void GetPhotosByAlbumAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void ResolveAlbumToAlbumWithPhotosDtoAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }

        [Fact()]
        public void ResolveAlbumDeletionAsyncTest()
        {
            Assert.True(false, "This test needs an implementation");
        }
    }
}